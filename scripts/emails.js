// students = [
//     ["Tasha Seng","mnguyen@codingdojo.com","pass"],
//     ["Angel Torres","mnguyen@codingdojo.com","pass"],
//     ["Banu Rai","mnguyen@codingdojo.com","fail(same code as another student)"],
//     ["bandhana rai","mnguyen@codingdojo.com","fail(same code as another student)"]
// ]

var students = [
    ["Matt Goff","matt@goff.us","pass"],
    ["Craig Bomba","craigbomba@yahoo.com","pass"],
    ["Warren Hellman","w.e.hellman@gmail.com","pass"],
    ["Laura Robinson","laurarachel@gmail.com","pass"],
    ["Akshay Raju","araju1@umbc.edu","pass"],
    ["bradley berning ","bradley_berning@yahoo.com","pass"],
    ["Matthew Cranford","mcranfordmail@gmail.com","pass"],
    ["Gurpal Bhoot","gsbhoot1020@gmail.com","pass"],
    ["Carlos Arambul","carlos_arambul@live.com","pass"],
    ["Kate Soliven","kate.c.soliven@gmail.com","fail"],
    ["Jairo L","jlbets123@gmail.com","pass"],
    ["Yaw Osei","Yaw.Osei@tamuc.edu","pass"],
    ["DAVID HO","davidhorph@hotmail.com","pass"],
    ["Young Kang","kyokjm@gmail.com","pass"],
    ["Radhika Mukund","radhika_mukund@hotmail.com","pass"],
    ["Bruce Barnett","hirebruce@comcast.net","fail"],
    ["Tasha Seng","sengwebb@gmail.com","pass"],
    ["Angel Torres","altorres367@gmail.com","pass"],
    ["Banu Rai","chuchib@gmail.com","fail(same code as another student)"],
    ["bandhana rai","rai.bandhana21@gmail.com","fail(same code as another student)"],
    ["Jenny Nguyen","jwnguyen01@gmail.com",],
    ["Robert Tuttle","ret@techie.com","pass"],
    ["Tariq Khan","digitalgnomes@gmail.com","pass"],
    ["SAMUEL GETACHEW","getachewsamuel3@gmail.com","fail"],
    ["Ron Allen Smith","ron@ronallensmith.com","fail"],
    ["Dave Chang","ipnetwork.expert@gmail.com","pass"],
    ["Francis Michael Galang","fmu_galang@yahoo.com","fail"],
    ["Carolyn Cartwright","carolyn.cartwright@gmail.com","pass"],
    ["Lionheart CHENG","lshcheng@gmail.com","fail"],
    ["Yaw Osei","Yaw.Osei@tamuc.edu","pass"],
    ["Yue Katou","yue.katou@gmail.com","pass"],
    ["Daniel Parag","daniel.parag@live.com","pass"],
    ["Lana Paulikava","pavlikova.lana@gmail.com","pass"],
    ["A Lee","lee.ays.lee@gmail.com","pass"],
    ["Neelaveni Lakshmanan","neelsvicky@gmail.com","pass"],
    ["Kavita Gupta","guptakavitas@yahoo.com","pass"],
    ["Svitlana Yaremenko","svitlanayr@gmail.com","pass"],
    ["V.N. E.","viktoria.evdokimova@gmail.com","fail"],
    ["wheels alert","wheelsalert@yahoo.com","fail"],
    ["Jonathan Abrams","jeyabrams@hotmail.com","fail"]
]
var nodemailer = require("nodemailer")

let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // secure:true for port 465, secure:false for port 587
    auth: {
        user: "mnguyen@codingdojo.com",
        pass: "Iamthecoolest1!"//set environment password first!
    }
});

sendMail(0)

function sendMail(i) { 
    var student = students[i]
    var instructorName = "Minh Nguyen"
    var email = "mnguyen@codingdojo.com"
    // create reusable transporter object using the default SMTP transport
    console.log(student, "student")
    let name = student[0].split(" ")[0]
    let score = student[2]
    // setup email data with unicode symbols
    if (score.indexOf("fail") != -1 ){
        var message = `Hello ${name}, 

I have graded your Intro to Python exam and unfortunately it was not a passing score. 

Due to the amount of students, I was unable to provide personal feedback at this time but 
if you would like more detailed feedback, please email me with your exam at mnguyen@codingdojo.com. 


Sincerely,
Instructor Minh Nguyen

PS. Sorry for the delay in this email and hope to hear from all of you!
        `
    }else{
        var message = `Hello ${name}, 
I have graded your Intro to Python exam and it was a passing score! 

If you would like more detailed feedback, please email me with your exam at mnguyen@codingdojo.com. 

Sincerely,
Instructor Minh Nguyen

PS. Sorry for the delay in this email and hope to hear from all of you!
        `
    }

    let mailOptions = {
        from: `${instructorName} ${email}`, // ${user.name} ${user.email}
        to: student[1], // change to student.email
        // to: "mnguyen@codingdojo.com",
        subject: "Intro To Python Exam", // Subject line
        text: message
    }
    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                res.send("Are you sure you input data correctly?");
            }
            if (i < students.length - 1) {            //  if the counter < 10, call the loop function
              sendMail(i+1);             //  ..  again which will trigger another 
            }else{  
                console.log("done")
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
    });
};