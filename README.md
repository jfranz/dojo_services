# INFO
This is a repository set up to run micro service mostly for online coding dojo program instructors

# SETUP
1. Be sure to update environment variables. Needed are: 
export CDEMAIL=mnguyen@codingdojo.com
export CDPASS=<codingdojo password>
export GMAILPASS=<gmail password> _for emailing_
export MMPASS=<mattermost password> _mattermost functionalities_

# Flightplan
``` 
plan.target('testing', {
    host: 'ec2-35-153-204-188.compute-1.amazonaws.com',
    username: 'ubuntu',
    agent: process.env.SSH_AUTH_SOCK,
    repository: 'https://gitlab.com/nguyenmp/dojo_services.git',
    branch: 'master',
    privateKey: '/Users/homefolder/Downloads/allec2.pem',
    maxDeploys: 10
});
```
