var express = require("express")
var app = express();
var request = require("request")
var bodyParser = require('body-parser')
var JSEncrypt = require('node-jsencrypt');
var crypt = new JSEncrypt({default_key_size: 2056});
var privateKey = crypt.getPrivateKey();
var publicKey = crypt.getPublicKey();
crypt.setKey(privateKey);
var nodemailer = require('nodemailer');
var parse = require('csv-parse')
var fs = require('fs');
var templates = require('./assets/templates.js');
var include_sig, belt, stack, template, user, subjectLine, email, password;
var channels = require("./assets/channels.json")
var auctions = require("./assets/currentData.json")
var readline = require('readline');
var google = require('googleapis');
var exec = require('child_process');


// CODE TO SERVE UP STATIC DATA TO localhost 8080
app.set('views', "./public");
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static('./public'))
// set the view engine to ejs
app.set('view engine', 'ejs');
app.get('/', function(req, res) {
    res.render('addmattermost.ejs',  {
        channels: channels
    });
});
app.post('/add', function(req, res) {
  if(req.body.names == "" || req.body.channels.length == 0) { 
    return res.redirect('/')
  }
  var names = req.body.names.split("\r\n")
  var add_to_channels = req.body.channels
  if(typeof(add_to_channels) == "string"){
    add_to_channels = [add_to_channels]
  }
  var getTokenOptions = {
    url:"https://dojo.news/api/v4/users/login",
    body:{"login_id": process.env["CDEMAIL"], "password":process.env["MMPASS"]},
    method: "POST",
    json: true
  };
  // GETS TOKEN

  request(getTokenOptions,  function (err, httpResponse, body) {
    if (err) {
      return console.error('upload failed:', err);
    }
    console.log(body)
    var response = httpResponse.toJSON()
    var token = response.headers.token
    var results = {}
    function getUser(i){
      var findNameOptions = {
        url:"https://dojo.news/api/v4/users/search",
        body:{ "term" : names[i] },
        method:"POST",
        json:true,
        headers: {
          'Authorization':"Bearer "+ token
        }
      };
      // GETS username
      request(findNameOptions,  function (err, httpResponse, body) {
        if (err) {
          return console.error('upload failed:', err);
        }
        console.log(body)
        if (body.length != 0){
          results[names[i]] = body[0].username
          function addToChannel(x){
            console.log(add_to_channels[x])
            var addToChannelOptions = {
              url:"https://dojo.news/api/v4/channels/" + add_to_channels[x] + "/members",
              headers: {
                'Authorization': "Bearer " + token
              },
              body: { "user_id" : body[0].id },
              json:true,
              method:"post",
            }
            request(addToChannelOptions,  function (err, httpResponse, body) {
              if(err){
                console.log(err)
                return
              }
              console.log(body)
              if (add_to_channels[x+1]){
                addToChannel(x+1)
              }else if (names[i+1]){
                getUser(i+1)
              }else{
                res.render("sucess", {emails:results})
              }
            })
          }
          addToChannel(0)
        }else{
          if (req.body.email == "on"){
            var sendEmailOptions = {
              url:"https://dojo.news/api/v4/teams/x1o444qmqiddjmjzz6ym5pocfh/invite/email",
              method:"POST",
              body:[names[i]],
              json:true,
              headers: {
                'Authorization':"Bearer "+ token
              }
            };
            request(sendEmailOptions, function (err, httpResponse, body) {
              console.log(body)
              results[names[i]] = "Email reminder sent"
              if (names[i+1]){
                getUser(i+1)
              }else{
                res.render("sucess", {emails:results})
              }
            })
          }else{
            console.log(results[names[i]])
            results[names[i]] = ""
            if (names[i+1]){
              getUser(i+1)
            }else{
              res.render("sucess", {emails:results})
            }
          }
        }
      })
    }
    getUser(0)
  });
});
app.post("/addChannel", (req, res)=>{
  var channel_name = req.body.channel_name

  var getTokenOptions = {
    url:"https://dojo.news/api/v4/users/login",
    body:{"login_id":  process.env["CDEMAIL"], "password":process.env["MMPASS"]},
    method: "POST",
    json: true
  };
  // GETS TOKEN

  request(getTokenOptions,  function (err, httpResponse, body) {
    if (err) {
      return console.error('upload failed:', err);
    }
    // console.log(httpResponse)
    var response = httpResponse.toJSON()
    console.log(response.headers.token)
    var token = response.headers.token
    var findChannelIdOptions = {
      url:'https://dojo.news/api/v4/teams/name/coding-dojo/channels/name/'+channel_name,
      method:"GET",
      json:true,
      headers: {
        'Authorization':"Bearer "+ token
      }
    };
    request(findChannelIdOptions,  function (err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      console.log(body)
      channels[channel_name] = body.id
      var json = JSON.stringify(channels);
      fs.writeFile('./assets/channels.json', json, 'utf8', ()=>{
        res.redirect("/")
      });
    })
  })
})
app.get('/grades', function(req, res) {
  res.render('sendemails', {publicKey:publicKey})
});
app.post('/sendgrades', function(req, res) {
  var email = req.body.email || process.env["CDEMAIL"]
  var password = req.body.password 
  password = crypt.decrypt(password)
  var instructorName = req.body.name || "Minh Nguyen"
  password = password ||  process.env["GMAILPASS"]
  var subject = req.body.subject || "Belt Exam Score"
  var belt = req.body.belt || "Belt"
  var data = req.body.data.split("\r\n")
  data = data.map(x=>x.split("\t"))
  var students = []
  var students_temp = pairArray(data[0])
  for (var i = 0; i < students_temp.length; i++) {
      var student = {email:students_temp[i][0], name:students_temp[i][1], comments:[]}
      students.push(student)
  }
  for(var i = 1; i < data.length; i++){
      for(var k = 0; k < data[i].length; k+=2){
        console.log(k)
          students[k/2].comments.push(data[i][k])
          students[k/2].comments.push(data[i][k+1])
      }
  }
  function pairArray(a) {
    var temp = a.slice();
    var arr = [];

    while (temp.length) {
      arr.push(temp.splice(0,2));
    }

    return arr;
  }
  var transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true, // secure:true for port 465, secure:false for port 587
      auth: {
          user: email,
          pass: password//set environment password first!
      }
  });
  sendMail(0)
  function sendMail(i) { 
      var student = students[i]
      // create reusable transporter object using the default SMTP transport
      console.log(student, "student")
      var name = student.name.split(" ")[0]
      var score = student.comments[student.comments.length-2]
      var table_data = ""
      var comments = pairArray(student.comments)
      for (var k = 0; k < comments.length-1; k++) {
          if(comments[k][1]){
              table_data += `<tr style="height: 21px;"><td style="padding: 2px 3px; vertical-align: bottom;">${comments[k][0]}</td><td style="padding: 2px 3px; vertical-align: bottom; word-wrap: break-word;">${comments[k][1].replace(/</g, '&lt;')}</td></tr>`
          }
      }
      var message = templates.grade(student.name.split(' ')[0], belt, score, table_data)
      // setup email data with unicode symbols
      console.log(student.email)
      var mailOptions = {
          from: `${instructorName} ${email}`, // ${user.name} ${user.email}
          to: student.email, // change to student.email
          // to: "mnguyen@codingdojo.com",
          subject: subject, // Subject line
          html: message// html body
      }
      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                  res.send("Are you sure you input data correctly?");
              }
              if (i < students.length - 1) {            //  if the counter < 10, call the loop function
                sendMail(i+1);             //  ..  again which will trigger another 
              }else{
                res.redirect("/sucess")
              }
              console.log('Message %s sent: %s', info.messageId, info.response);
      });
  };

});
app.get("/sucess", function(req, res){
  res.send("sucess")
})
app.get("/emails", function(req, res){
  res.render("emailer.ejs",{publicKey:publicKey})
})
app.post("/emails", function(req, res){
  var email = req.body.email || process.env["CDEMAIL"]
  var password = req.body.password 
  password = crypt.decrypt(password)
  var instructorName = req.body.name || "Minh Nguyen"
  password = password || process.env["GMAILPASS"]
  var subject = req.body.subject || "Update"
  var emails = req.body.emails.split("\r\n")
  console.log(emails)
  emails = emails.map(x=>x.split("\t"))
  var content = req.body.content
  var students = []
  for (var i = 0; i < emails.length; i++) {
      var student = {email:emails[i][1], name:emails[i][0]}
      students.push(student)
  }
  console.log(students)
  var transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true, // secure:true for port 465, secure:false for port 587
      auth: {
          user: email,
          pass: password//set environment password first!
      }
  });
  sendMail(0)


  function sendMail(i) { 
      var student = students[i]
      // create reusable transporter object using the default SMTP transport
      console.log(student, "student")
      var name = student.name.split(" ")[0]
      var mailOptions = {
          from: `${instructorName} ${email}`, // ${user.name} ${user.email}
          to: student.email, // change to student.email
          // to: "mnguyen@codingdojo.com",
          subject: subject, // Subject line
      }
      if(!content){
        mailOptions.html = templates.update(name, instructorName.split(" ")[0])
      }else{
        mailOptions.text = content.replace(/({name})/g, name).replace(/({instructor_first_name})/g, instructorName.split(" ")[0])
      }
      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                  res.send("Are you sure you input data correctly?");
              }
              if (i < students.length - 1) {            //  if the counter < 10, call the loop function
                sendMail(i+1);             //  ..  again which will trigger another 
              }else{
                res.redirect("/sucess")
              }
              console.log('Message %s sent: %s', info.messageId, info.response);
      });
  };
})
app.get("/testpage", function(req,res){
  res.render("test")
})

require("./routes/crawlers.js")(app)
require("./routes/auctions.js")(app)
require("./routes/github.js")(app)
app.listen(3000, function(){
  console.log("listening on 3000")
})