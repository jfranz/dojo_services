var exports = module.exports = function(app){
  app.post("/addChannel", (req, res)=>{
    var channel_name = req.body.channel_name
  
    var getTokenOptions = {
      url:"https://dojo.news/api/v4/users/login",
      body:{"login_id":  process.env["CDEMAIL"], "password":process.env["MMPASS"]},
      method: "POST",
      json: true
    };
    // GETS TOKEN
  
    request(getTokenOptions,  function (err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      // console.log(httpResponse)
      var response = httpResponse.toJSON()
      console.log(response.headers.token)
      var token = response.headers.token
      var findChannelIdOptions = {
        url:'https://dojo.news/api/v4/teams/name/coding-dojo/channels/name/'+channel_name,
        method:"GET",
        json:true,
        headers: {
          'Authorization':"Bearer "+ token
        }
      };
      request(findChannelIdOptions,  function (err, httpResponse, body) {
        if (err) {
          return console.error('upload failed:', err);
        }
        console.log(body)
        channels[channel_name] = body.id
        var json = JSON.stringify(channels);
        fs.writeFile('./assets/channels.json', json, 'utf8', ()=>{
          res.redirect("/")
        });
      })
    })
  })
  app.get('/grades', function(req, res) {
    res.render('sendemails', {publicKey:publicKey})
  });
  app.post('/sendgrades', function(req, res) {
    var email = req.body.email || process.env["CDEMAIL"]
    var password = req.body.password 
    password = crypt.decrypt(password)
    var instructorName = req.body.name || "Minh Nguyen"
    password = password ||  process.env["GMAILPASS"]
    var subject = req.body.subject || "Belt Exam Score"
    var belt = req.body.belt || "Belt"
    var data = req.body.data.split("\r\n")
    data = data.map(x=>x.split("\t"))
    var students = []
    var students_temp = pairArray(data[0])
    for (var i = 0; i < students_temp.length; i++) {
        var student = {email:students_temp[i][0], name:students_temp[i][1], comments:[]}
        students.push(student)
    }
    for(var i = 1; i < data.length; i++){
        for(var k = 0; k < data[i].length; k+=2){
          console.log(k)
            students[k/2].comments.push(data[i][k])
            students[k/2].comments.push(data[i][k+1])
        }
    }
    function pairArray(a) {
      var temp = a.slice();
      var arr = [];
  
      while (temp.length) {
        arr.push(temp.splice(0,2));
      }
  
      return arr;
    }
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // secure:true for port 465, secure:false for port 587
        auth: {
            user: email,
            pass: password//set environment password first!
        }
    });
    sendMail(0)
    function sendMail(i) { 
        var student = students[i]
        // create reusable transporter object using the default SMTP transport
        console.log(student, "student")
        var name = student.name.split(" ")[0]
        var score = student.comments[student.comments.length-2]
        var table_data = ""
        var comments = pairArray(student.comments)
        for (var k = 0; k < comments.length-1; k++) {
            if(comments[k][1]){
                table_data += `<tr style="height: 21px;"><td style="padding: 2px 3px; vertical-align: bottom;">${comments[k][0]}</td><td style="padding: 2px 3px; vertical-align: bottom; word-wrap: break-word;">${comments[k][1].replace(/</g, '&lt;')}</td></tr>`
            }
        }
        var message = templates.grade(student.name.split(' ')[0], belt, score, table_data)
        // setup email data with unicode symbols
        console.log(student.email)
        var mailOptions = {
            from: `${instructorName} ${email}`, // ${user.name} ${user.email}
            to: student.email, // change to student.email
            // to: "mnguyen@codingdojo.com",
            subject: subject, // Subject line
            html: message// html body
        }
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    res.send("Are you sure you input data correctly?");
                }
                if (i < students.length - 1) {            //  if the counter < 10, call the loop function
                  sendMail(i+1);             //  ..  again which will trigger another 
                }else{
                  res.redirect("/sucess")
                }
                console.log('Message %s sent: %s', info.messageId, info.response);
        });
    };
  
  });
}