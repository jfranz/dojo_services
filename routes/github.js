var exports = module.exports = function(app){
  app.post("/githubadd", function(req, res){
    var auth_token = require("./assets/github_cred.json")[req.body.channel_id] || "04a41ffd173a4479cda580c463960f2b00da66d1"
    var data = req.body.text.split(" ")
    var orgName = data[0]
    var username = data[1]
    var url = "https://api.github.com/orgs/" + orgName + "/memberships/" + username
    console.log(url)
    var options = {
      url:url,
      method:"PUT",
      headers: {
        'User-Agent':"nguyenhmp",
        'Authorization': 'token '+ auth_token
      },
    };
    request(options, function (err, httpResponse, body) {
      if (err){
        console.log("err", err)
        res.json({"icon_url": "https://assets-cdn.github.com/images/modules/logos_page/GitHub-Mark.png", "response_type": "ephemeral", "text": "An **ERROR** has occurred. Please contact your instructor to be added to the org."})
      }else{
        console.log(JSON.parse(body).message)
        body = JSON.parse(body)
        if(body.message == "Not Found" || body.message == "Bad credentials"){
          res.json({"icon_url": "https://assets-cdn.github.com/images/modules/logos_page/GitHub-Mark.png", "response_type": "ephemeral", "text": "An **ERROR** has occurred. Please contact your instructor to be added to the org."})
        }else{
          res.json({"icon_url": "https://assets-cdn.github.com/images/modules/logos_page/GitHub-Mark.png","response_type": "in_channel", "text": `_${username}_ has been added to **${orgName}**`})
        }
      }
    })
  })
}